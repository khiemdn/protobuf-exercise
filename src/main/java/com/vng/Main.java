package com.vng;

import com.google.protobuf.InvalidProtocolBufferException;
import com.vng.proto.AddressBookProto.*;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

import com.google.protobuf.util.JsonFormat;
public class Main {

    public static void main(String[] args) {
        Person john = Person.newBuilder()
                .setName("John")
                .setGroup("a123")
                .addEmail("john@gmail.com")
                .addEmail("john@hotmail.com")
                .addPhone(Person.Phone.newBuilder().setGroup("vn1").setNumber("08884522545"))
                .addPhone(Person.Phone.newBuilder().setGroup("vn1").setNumber("08884522533"))
                .addPhone(Person.Phone.newBuilder().setGroup("vn1").setNumber("08884522522"))
                .addNote("john phone")
                .addNote("vn1 group")
                .build();
        Person mary = Person.newBuilder()
                .setName("Mary")
                .setGroup("b123")
                .addEmail("mary@gmail.com")
                .addPhone(Person.Phone.newBuilder().setGroup("vn2").setNumber("5622012156"))
                .addPhone(Person.Phone.newBuilder().setGroup("vn2").setNumber("5622012157"))
                .addNote("mary phone")
                .addNote("vn2 group")
                .build();
        Person chris = Person.newBuilder()
                .setName("Chris")
                .setGroup("c123")
                .addEmail("chris@ggg.com")
                .addPhone(Person.Phone.newBuilder().setGroup("vn3").setNumber("5622012156"))
                .addPhone(Person.Phone.newBuilder().setGroup("vn1").setNumber("0888852154"))
                .addNote("chris phone")
                .addNote("vn1 group")
                .addNote("vn2 group")
                .build();

        AddressBook addressBook;
        addressBook = AddressBook.newBuilder()
                .addPeople(john)
                .addPeople(mary)
                .addPeople(chris)
                .build();

        String json = "";
        // convert addressbook to json
        try {
            json = JsonFormat.printer().print(addressBook);
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
        }
        // write json to file
        try {
            FileOutputStream fout = new FileOutputStream("output.txt");
            FileOutputStream foutBin = new FileOutputStream("output.bin");

            PrintWriter printWriter = new PrintWriter(fout);
            printWriter.print(json);
            addressBook.writeTo(foutBin);

            printWriter.close();
            fout.close();
            foutBin.close();
            System.out.println("success...");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        // read back from file and convert to addressbook
        try{
            FileInputStream fin = new FileInputStream("output.bin");
            AddressBook addressBookRead = AddressBook.parseFrom(fin);
            String readJSON = JsonFormat.printer().print(addressBookRead.toBuilder());
            System.out.println(readJSON);
            fin.close();
        } catch (Exception e){
            System.out.println(e.getMessage());
        }

    }
}
